# Unit test

## Installation steps

1. Clone repository `git clone`
2. Install dependencies
3. Start the app

## Terminal commands

```sh
git clone https://gitlab.com/arttu.lahtinen/unit-test
cd ./unit-test
npm i
```
Start cmd: `npm run start`

Start tests: `npm run test`

